package br.com.senac.beautysys.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;

import java.util.List;

import adapter.AdapterServico;
import br.com.senac.beautysys.R;
import br.com.senac.beautysys.model.Servico;

public class ListaEsteticaActivity extends AppCompatActivity {

    //private ArrayList<String> lista = new ArrayList<>();

    private List<Servico>  listaEstetica ;
    private AdapterServico adapterEstetica ;

    CheckBox checkBox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_estetica);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarEstetica);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.botaoEstetica);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        /* adionando intens na lista
        lista.add("Estetica");
        lista.add("Estetica");
        lista.add("Estetica");

        // definindo o adptar
        // apfasdfsdfksdf

        int layout = android.R.layout.simple_list_item_1;

        //das ddasd
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this , layout , lista ) ;

       ListView listViewPrecos = (ListView)findViewById(R.id.lista_servico_estetica) ;

       listViewPrecos.setAdapter(adapter);*/

        Intent intent = getIntent() ;

        listaEstetica = (List<Servico>)intent.getSerializableExtra(MainActivity.LISTA_SERVICO_ESTETICA) ;

        adapterEstetica = new AdapterServico(this,listaEstetica);

        ListView listViewServicos = (ListView) findViewById(R.id.lista_servico_estetica) ;

        listViewServicos.setAdapter(adapterEstetica);


    }

}
