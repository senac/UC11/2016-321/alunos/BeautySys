package br.com.senac.beautysys.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;

import java.util.List;

import adapter.AdapterServico;
import br.com.senac.beautysys.R;
import br.com.senac.beautysys.model.Servico;

public class ListaCabeloActivity extends AppCompatActivity {

    private List<Servico>  listaCabelo ;
    private AdapterServico adapterCabelo ;

    CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_cabelo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarCabelo);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.botaoCabelo);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ListaCabeloActivity.this, MainActivity.class);


                startActivity(intent);




               /* Snackbar.make(view, "Eu acabei de clicar aqui", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });


        Intent intent = getIntent() ;

        listaCabelo = (List<Servico>)intent.getSerializableExtra(MainActivity.LISTA_SERVICO_CABELO) ;

        adapterCabelo = new AdapterServico(this,listaCabelo);

        ListView listViewServicos = (ListView) findViewById(R.id.lista_servico_cabelo) ;

        listViewServicos.setAdapter(adapterCabelo);



        CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);



        }

    }


