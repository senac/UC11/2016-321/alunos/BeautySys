package br.com.senac.beautysys.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.senac.beautysys.R;
import br.com.senac.beautysys.model.Servico;

public class MainActivity extends AppCompatActivity {

    public static final String LISTA_SERVICO_CABELO = "lista_Cabelo" ;
    public static final String LISTA_SERVICO_UNHA = "Lista_Unha" ;
    public static final String LISTA_SERVICO_ESTETICA = "Lista_Estetica";
    public static final String LISTA_SERVICO_TODOS = "Lista_Todos";


    private FrameLayout conteudo;

    public static List<Servico> listaTodosServicos = new ArrayList<>();


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            conteudo.removeAllViews();

            switch (item.getItemId()) {
                case R.id.navigation_produto:
                    conteudo.addView(getLayoutInflater().inflate(R.layout.produto , conteudo , false) );
                    return true;
                case R.id.navigation_servico:
                    conteudo.addView(getLayoutInflater().inflate(R.layout.servico , conteudo , false) );
                    return true;
                case R.id.navigation_outros:
                    conteudo.addView(getLayoutInflater().inflate(R.layout.outro , conteudo , false) );
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        conteudo = (FrameLayout) findViewById(R.id.content);
        conteudo.addView(getLayoutInflater().inflate(R.layout.produto , conteudo , false) );
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

      // Chamando a listas criadas (Cabelo, Unhas, Estetica e a todos)...

       adicionarNaLista();



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_atendimento, menu);

        return true;


    }


    public void cabelo(View view){
        Intent intent = new Intent(this , ListaCabeloActivity.class) ;

        List<Servico> listaCabelo = new ArrayList<Servico>() ;

        for(Servico s : listaTodosServicos){
            if(s.getCategoria().equals("Cabelo")){
                listaCabelo.add(s);
            }
        }

        intent.putExtra(MainActivity.LISTA_SERVICO_CABELO, (Serializable) listaCabelo);

        startActivity(intent);
    }

    public void unhas(View view){
        Intent intent = new Intent(this , ListaUnhasActivity.class) ;

        List<Servico> listaUnha = new ArrayList<Servico>();

        for (Servico s : listaTodosServicos){
            if(s.getCategoria().equals("Unha")){
                listaUnha.add(s);

            }
        }

        intent.putExtra(MainActivity.LISTA_SERVICO_UNHA, (Serializable) listaUnha);


        startActivity(intent);
    }

    public void estetica(View view){
        Intent intent = new Intent(this , ListaEsteticaActivity.class) ;

        List<Servico> listaEstetica = new ArrayList<Servico>();

        for (Servico s : listaTodosServicos){
            if(s.getCategoria().equals("Estetica")){
                listaEstetica.add(s);

            }
        }

        intent.putExtra(MainActivity.LISTA_SERVICO_ESTETICA, (Serializable) listaEstetica);


        startActivity(intent);
    }

    public void listaTodos(){
        Intent intent = new Intent(this , ListaTodosServicosActivity.class) ;

        List<Servico> listaTodos = new ArrayList<Servico>();

        for (Servico s : listaTodosServicos){
            if(s.getCategoria().equals("Estetica"))
            if(s.getCategoria().equals("Cabelo"))
            if(s.getCategoria().equals("Unha")){

                {
                    {
                        listaTodos.add(s);
                        listaTodosServicos.add((Servico) listaTodos);

                    }
                }
            }
        }

        intent.putExtra(MainActivity.LISTA_SERVICO_TODOS, (Serializable) listaTodos);


        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //adicionando no menu a intent para activity ListaTodosServicos...

        Intent intent = null;

        switch (item.getItemId()){

            case R.id.action_ListaTodos:
                 intent = new Intent(this, ListaTodosServicosActivity.class);

                break;
        }

        startActivity(intent);
        Toast.makeText(this, "clicado", Toast.LENGTH_LONG).show();



        return true;
    }

    public void adicionarNaLista (){

        Servico penteado = new Servico();
        penteado.setDescricao("Penteado");
        penteado.setCategoria("Cabelo");
        penteado.setPreco(35);
        listaTodosServicos.add(penteado);

        Servico coloracao = new Servico();
        coloracao.setDescricao("Coloracao");
        coloracao.setCategoria("Cabelo");
        coloracao.setPreco(28);
        listaTodosServicos.add(coloracao);


        // Serviços de Unhas;

        Servico manicure = new Servico();
        manicure.setDescricao("Manicure");
        manicure.setCategoria("Unha");
        manicure.setPreco(15);
        listaTodosServicos.add(manicure);


        Servico unhaGel = new Servico();
        unhaGel.setDescricao("Unha gel");
        unhaGel.setCategoria("Unha");
        unhaGel.setPreco(35);
        listaTodosServicos.add(unhaGel);



        // Serviços de Estetica;
        Servico maquiagem = new Servico();
        maquiagem.setDescricao("Maquiagem");
        maquiagem.setCategoria("Estetica");
        maquiagem.setPreco(45);
        listaTodosServicos.add(maquiagem);

        Servico sobrancelha = new Servico();
        sobrancelha.setDescricao("Sobrancelha");
        sobrancelha.setCategoria("Estetica");
        sobrancelha.setPreco(20);
        listaTodosServicos.add(sobrancelha);





        /*listaTodosServicos.add(manicure);
        listaTodosServicos.add(unhaGel);



        //Lista de Serviços(Estetica)
        listaTodosServicos.add(maquiagem);
        listaTodosServicos.add(sobrancelha);
*/

    }




}
