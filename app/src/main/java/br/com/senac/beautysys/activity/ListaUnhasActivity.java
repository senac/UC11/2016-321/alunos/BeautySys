package br.com.senac.beautysys.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;

import java.util.List;

import adapter.AdapterServico;
import br.com.senac.beautysys.R;
import br.com.senac.beautysys.model.Servico;

public class ListaUnhasActivity extends AppCompatActivity {

    //private ArrayList<String> listaUnha = new ArrayList<>();

    private List<Servico> listaUnha;
    private AdapterServico adapterUnha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_unhas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarUnha);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.botaoUnha);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


       /* listaUnha.add("Unhas");
        listaUnha.add("Unhas");
        listaUnha.add("Unhas");
        listaUnha.add("Unhas");
        listaUnha.add("Unhas");
        listaUnha.add("Unhas");
        listaUnha.add("Unhas");
        listaUnha.add("Unhas");
*/

        //int layout = android.R.layout.simple_list_item_1;

       // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this , layout , listaUnha ) ;

       // ListView listViewPrecos = (ListView)findViewById(R.id.lista_servico_unhas) ;

       // listViewPrecos.setAdapter(adapter);


        Intent intent = getIntent() ;
        listaUnha = (List<Servico>)intent.getSerializableExtra(MainActivity.LISTA_SERVICO_UNHA) ;

        adapterUnha = new AdapterServico(this,listaUnha);

        ListView listViewServicos = (ListView) findViewById(R.id.lista_servico_unhas) ;

        listViewServicos.setAdapter(adapterUnha);



        CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);



    }

}
