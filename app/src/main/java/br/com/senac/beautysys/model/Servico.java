package br.com.senac.beautysys.model;

import android.graphics.drawable.Drawable;

import java.io.Serializable;
import java.text.NumberFormat;

/**
 * Created by sala304b on 21/09/2017.
 */

public class Servico implements Serializable{

    private NumberFormat nf = NumberFormat.getCurrencyInstance();

    private int codigo;
    private String descricao ;
    private String categoria ;
    private double preco;

    public NumberFormat getNf() {
        return nf;
    }

    public void setNf(NumberFormat nf) {
        this.nf = nf;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getPrecoFormatado(){
        return nf.format(this.preco) ;
    }
}
