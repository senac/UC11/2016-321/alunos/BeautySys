package br.com.senac.beautysys.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.ListView;

import java.util.List;

import adapter.AdapterServico;
import br.com.senac.beautysys.R;
import br.com.senac.beautysys.model.Servico;

public class ListaTodosServicosActivity extends AppCompatActivity {

    private List<Servico> listaTodos;
    private AdapterServico adapterListaTodos;
    private CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_todos_servicos);


        Intent intent = getIntent() ;
       listaTodos = (List<Servico>)intent.getSerializableExtra(MainActivity.LISTA_SERVICO_TODOS) ;

        adapterListaTodos = new AdapterServico(this,MainActivity.listaTodosServicos);

        ListView listViewServicos = (ListView) findViewById(R.id.lista_servico_Todos) ;

        listViewServicos.setAdapter(adapterListaTodos);



       checkBox = (CheckBox) findViewById(R.id.checkBox);


    }
}
