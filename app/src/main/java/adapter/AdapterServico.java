package adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.senac.beautysys.R;
import br.com.senac.beautysys.model.Servico;

/**
 * Created by sala304b on 21/09/2017.
 */

public class AdapterServico extends BaseAdapter {

    private List<Servico> lista;
    private Activity contexto;

    private static  final String CABELO = "Cabelo" ;
    private static  final String UNHA = "Unha" ;
    private static final String ESTETICA = "Estetica";


    public AdapterServico(Activity contexto , List<Servico> lista) {
        this.lista = lista;
        this.contexto = contexto;
    }



    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int i) {
        return lista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int posicao, View convertview, ViewGroup parent) {

        View view = contexto.getLayoutInflater()
                .inflate(R.layout.item_servico, parent , false);

        Servico servico = this.lista.get(posicao);

        ImageView imageView = (ImageView) view.findViewById(R.id.image_servico) ;
        TextView textViewNome = (TextView) view.findViewById(R.id.nome_servico);
        TextView textViewPreco = (TextView) view.findViewById(R.id.preco_servico);



        String imagem = servico.getCategoria() ;

        switch (imagem){
            case CABELO :
                imageView.setImageResource(R.drawable.cabelo);
                break;

            case UNHA :
                imageView.setImageResource(R.drawable.unhas);
                break;

            case ESTETICA :
                imageView.setImageResource(R.drawable.estetica);
                break;


            default:

                // sem imagem
                imageView.setImageResource(R.drawable.ic_notifications_black_24dp);

        }



        textViewNome.setText(servico.getDescricao());
        textViewPreco.setText(servico.getPrecoFormatado());





        return view;
    }
}
